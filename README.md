# iMedic Info

This is an informational Android application of iMEDIC International Medical Interdisciplinary Congress held annually by the Collegium Medicum in Bydgoszcz - Poland.
Within the app users can find up to date informations about the congress itself, contacts to most important organizers, rules and the interactive map of the building where the conference takes place.

## Project status

 - [x] Finished - requires minor polishing

## Installation

You can download the application from [Google Play Store](https://play.google.com/store/apps/details?id=amadeo.imedicinfo) or you can download the sources and build it yourself.

## Built With

* [Android Studio](https://developer.android.com/studio/)
* [Gradle](https://gradle.org/)

## Authors

Paweł Wojtczak

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details

## Note

App was designed to be an improved version of the original [iMedic Info](https://bitbucket.org/amadeoalex/imedic-info/src/master/) project. Overall the project was completed successfully although due to the other more important matters (app was just a sugar on top of the conference) involving the congress I lacked the time to make the final push for the app. If everything goes fine, app will be used during the iMedic Congress 2020.