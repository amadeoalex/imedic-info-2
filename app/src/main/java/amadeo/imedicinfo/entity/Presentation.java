package amadeo.imedicinfo.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "presentation_table")
public class Presentation {

    public static final String TYPE_ORAL = "Oral";
    public static final String TYPE_POSTER = "Poster";
    public static final String TYPE_ALL = "All sessions";

    public static final String BLOCK_MEDICAL = "medical";
    public static final String BLOCK_PHARMACEUTICAL = "pharmaceutical";
    public static final String BLOCK_HEALTH = "health";

    @PrimaryKey
    @NonNull
    private String id;

    private String session;
    private String sessionType;
    private String title;
    private String author;
    private String presentationAbstract;
    private String presentationTime;
    private String block;

    public Presentation(@NonNull String id, String session, String sessionType, String title, String author, String presentationAbstract, String presentationTime, String block) {
        this.id = id;
        this.session = session;
        this.sessionType = sessionType;
        this.title = title;
        this.author = author;
        this.presentationAbstract = presentationAbstract;
        this.presentationTime = presentationTime;
        this.block = block;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public String getSessionType() {
        return sessionType;
    }

    public String getPresentationAbstract() {
        return presentationAbstract;
    }

    public void setPresentationAbstract(String presentationAbstract) {
        this.presentationAbstract = presentationAbstract;
    }

    public String getPresentationTime() {
        return presentationTime;
    }

    public void setPresentationTime(String presentationTime) {
        this.presentationTime = presentationTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBlock(){
        return block;
    }

    public void setBlock(String block){
        this.block = block;
    }
}
