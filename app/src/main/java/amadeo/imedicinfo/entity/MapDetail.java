package amadeo.imedicinfo.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "map_details_table")
public class MapDetail {
    @PrimaryKey
    @NonNull
    private String id;

    private String title;
    private String message;

    public MapDetail(@NonNull String id, String title, String message) {
        this.id = id;
        this.title = title;
        this.message = message;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}