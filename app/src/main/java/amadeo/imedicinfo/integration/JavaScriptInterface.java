package amadeo.imedicinfo.integration;

import android.app.Activity;
import android.util.Log;
import android.webkit.JavascriptInterface;

import java.lang.ref.WeakReference;

import amadeo.imedicinfo.MainActivity;
import amadeo.imedicinfo.R;
import amadeo.imedicinfo.entity.MapDetail;
import amadeo.imedicinfo.ui.fragment.MapDetailDialogFragment;
import amadeo.imedicinfo.viewmodel.MapViewModel;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

public class JavaScriptInterface {

    private final WeakReference<Activity> activityWeakReference;
    private final MapViewModel mapViewModel;

    public JavaScriptInterface(FragmentActivity fragmentActivity, MapViewModel mapViewModel) {
        this.activityWeakReference = new WeakReference<>(fragmentActivity);
        this.mapViewModel = mapViewModel;
    }

    @JavascriptInterface
    public void mapInteraction(String objectId) {
        Log.i("iMedic", "click");
        //Toast.makeText(activityWeakReference.get(), objectId, Toast.LENGTH_SHORT).show();

        MainActivity mainActivity = (MainActivity) activityWeakReference.get();

        MapDetail mapDetail = mapViewModel.getMapDetail(objectId);
        String title = mapDetail != null ? mapDetail.getTitle() : mainActivity.getResources().getString(R.string.sorry);
        String message = mapDetail != null ? mapDetail.getMessage() : mainActivity.getResources().getString(R.string.no_info);

        FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();
        MapDetailDialogFragment dialogFragment = MapDetailDialogFragment.newInstance(title, message);
        dialogFragment.show(fragmentManager, "map_detail");
    }
}
