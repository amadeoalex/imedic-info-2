package amadeo.imedicinfo.viewmodel;

import android.app.Application;

import amadeo.imedicinfo.datebase.MapDetailDatabase;
import amadeo.imedicinfo.entity.MapDetail;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class MapViewModel extends AndroidViewModel {
    private final MapDetailDatabase mapDetailDatabase;

    public MapViewModel(@NonNull Application application) {
        super(application);

        mapDetailDatabase = MapDetailDatabase.getInstance(application);
    }

    public MapDetail getMapDetail(String id){
        return mapDetailDatabase.mapDetailDao().getMapDetail(id);
    }

    public void deleteAll(){
        mapDetailDatabase.mapDetailDao().deleteAllMapDetails();
    }
}

