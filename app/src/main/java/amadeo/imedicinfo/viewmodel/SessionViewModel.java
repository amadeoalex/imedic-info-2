package amadeo.imedicinfo.viewmodel;

import android.app.Application;

import java.util.List;

import amadeo.imedicinfo.entity.Presentation;
import amadeo.imedicinfo.repository.PresentationRepository;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class SessionViewModel extends AndroidViewModel {
    private final PresentationRepository presentationRepository;

    public SessionViewModel(@NonNull Application application) {
        super(application);

        presentationRepository = new PresentationRepository(application);
    }

    public void insert(Presentation presentation){
        presentationRepository.insert(presentation);
    }

    public void update(Presentation presentation){
        presentationRepository.update(presentation);
    }

    public void delete(Presentation presentation){
        presentationRepository.delete(presentation);
    }

    public void deleteAll(){
        presentationRepository.deleteAllPresentations();
    }

    public LiveData<List<Presentation>> getPresentations(){
        return presentationRepository.getPresentations();
    }

    public LiveData<List<Presentation>> getPresentationsOfType(String sessionType){
        return  presentationRepository.getPresentationsOfType(sessionType);
    }

    public LiveData<List<String>> getSessions(){
        return  presentationRepository.getSessions();
    }

    public LiveData<List<String>> getSessions(String sessionType){
        return  presentationRepository.getSessions(sessionType);
    }
}
