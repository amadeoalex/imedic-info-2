package amadeo.imedicinfo.viewmodel;

import android.app.Application;

import java.util.List;

import amadeo.imedicinfo.entity.News;
import amadeo.imedicinfo.repository.NewsRepository;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class HomeViewModel extends AndroidViewModel {
    private final NewsRepository newsRepository;
    public final LiveData<List<News>> news;

    public HomeViewModel(@NonNull Application application) {
        super(application);

        newsRepository = new NewsRepository(application);
        news = newsRepository.getNews();
    }

    public void insert(News news){
        newsRepository.insert(news);
    }

    public void update(News news){
        newsRepository.update(news);
    }

    public void delete(News news){
        newsRepository.delete(news);
    }

    public void deleteAll(){
        newsRepository.deleteAllNews();
    }
}
