package amadeo.imedicinfo.repository;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import amadeo.imedicinfo.dao.PresentationDao;
import amadeo.imedicinfo.datebase.PresentationDatabase;
import amadeo.imedicinfo.entity.Presentation;
import androidx.lifecycle.LiveData;

public class PresentationRepository {
    private final PresentationDao presentationDao;

    private final LiveData<List<Presentation>> presentations;
    private final LiveData<List<Presentation>> oralPresentations;
    private final LiveData<List<Presentation>> posterPresentations;
    private final LiveData<List<String>> sessions;
    private final LiveData<List<String>> oralSessions;
    private final LiveData<List<String>> posterSessions;


    public PresentationRepository(Application application) {
        PresentationDatabase presentationDatabase = PresentationDatabase.getInstance(application);
        presentationDao = presentationDatabase.presentationDao();

        presentations = presentationDao.getPresentations();
        oralPresentations = presentationDao.getPresentationsOfType(Presentation.TYPE_ORAL);
        posterPresentations = presentationDao.getPresentationsOfType(Presentation.TYPE_POSTER);
        sessions = presentationDao.getSessions();
        oralSessions = presentationDao.getSessions(Presentation.TYPE_ORAL);
        posterSessions = presentationDao.getSessions(Presentation.TYPE_POSTER);
    }

    public void insert(Presentation presentation) {
        new InsertAsyncTask(presentationDao).execute(presentation);
    }

    public void update(Presentation presentation) {
        new InsertAsyncTask(presentationDao).execute(presentation);
    }

    public void delete(Presentation presentation) {
        new DeleteAsyncTask(presentationDao).execute(presentation);
    }

    public void deleteAllPresentations() {
        new DeleteAllAsyncTask(presentationDao).execute();
    }

    public LiveData<List<Presentation>> getPresentations() {
        return presentations;
    }

    //TODO: consider using enum and type converters
    public LiveData<List<Presentation>> getPresentationsOfType(String sessionType) {
        switch (sessionType) {
            case Presentation.TYPE_ORAL:
                return oralPresentations;
            case Presentation.TYPE_POSTER:
                return posterPresentations;
            case Presentation.TYPE_ALL:
                return presentations;
            default:
                throw new IllegalArgumentException("provided session type is not valid " + sessionType);
        }
    }

    public LiveData<List<String>> getSessions() {
        return sessions;
    }

    public LiveData<List<String>> getSessions(String sessionType) {
        switch (sessionType) {
            case Presentation.TYPE_ORAL:
                return oralSessions;
            case Presentation.TYPE_POSTER:
                return posterSessions;
            default:
                throw new IllegalArgumentException("provided session type is not valid " + sessionType);
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<Presentation, Void, Void> {
        private final PresentationDao presentationDao;

        private UpdateAsyncTask(PresentationDao presentationDao) {
            this.presentationDao = presentationDao;
        }

        @Override
        protected Void doInBackground(Presentation... presentations) {
            presentationDao.update(presentations[0]);
            return null;
        }
    }

    private static class InsertAsyncTask extends AsyncTask<Presentation, Void, Void> {
        private final PresentationDao presentationDao;

        private InsertAsyncTask(PresentationDao presentationDao) {
            this.presentationDao = presentationDao;
        }

        @Override
        protected Void doInBackground(Presentation... presentations) {
            presentationDao.insert(presentations[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<Presentation, Void, Void> {
        private final PresentationDao presentationDao;

        private DeleteAsyncTask(PresentationDao presentationDao) {
            this.presentationDao = presentationDao;
        }

        @Override
        protected Void doInBackground(Presentation... presentations) {
            presentationDao.delete(presentations[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncTask extends AsyncTask<Presentation, Void, Void> {
        private final PresentationDao presentationDao;

        private DeleteAllAsyncTask(PresentationDao presentationDao) {
            this.presentationDao = presentationDao;
        }

        @Override
        protected Void doInBackground(Presentation... presentations) {
            presentationDao.deleteAllPresentations();
            return null;
        }
    }
}
