package amadeo.imedicinfo.repository;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import amadeo.imedicinfo.dao.NewsDao;
import amadeo.imedicinfo.datebase.NewsDatabase;
import amadeo.imedicinfo.entity.News;
import androidx.lifecycle.LiveData;

public class NewsRepository {
    private final NewsDao newsDao;
    private final LiveData<List<News>> news;

    public NewsRepository(Application application) {
        NewsDatabase database = NewsDatabase.getInstance(application);
        newsDao = database.newsDao();
        news = newsDao.getNews();
    }

    public void insert(News news) {
        new InsertAsyncTask(newsDao).execute(news);
    }

    public void update(News news) {
        new UpdateAsyncTask(newsDao).execute(news);
    }

    public void delete(News news) {
        new DeleteAsyncTask(newsDao).execute(news);
    }

    public void deleteAllNews() {
        new DeleteAllAsyncTask(newsDao).execute();
    }

    public LiveData<List<News>> getNews() {
        return news;
    }

    private static class InsertAsyncTask extends AsyncTask<News, Void, Void> {
        private final NewsDao newsDao;

        private InsertAsyncTask(NewsDao newsDao) {
            this.newsDao = newsDao;
        }

        @Override
        protected Void doInBackground(News... news) {
            newsDao.insert(news[0]);
            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<News, Void, Void> {
        private final NewsDao newsDao;

        private UpdateAsyncTask(NewsDao newsDao) {
            this.newsDao = newsDao;
        }

        @Override
        protected Void doInBackground(News... news) {
            newsDao.update(news[0]);
            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<News, Void, Void> {
        private final NewsDao newsDao;

        private DeleteAsyncTask(NewsDao newsDao) {
            this.newsDao = newsDao;
        }

        @Override
        protected Void doInBackground(News... news) {
            newsDao.delete(news[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private final NewsDao newsDao;

        private DeleteAllAsyncTask(NewsDao newsDao) {
            this.newsDao = newsDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            newsDao.deleteAllNews();
            return null;
        }
    }
}
