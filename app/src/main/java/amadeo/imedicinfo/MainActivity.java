package amadeo.imedicinfo;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;

import amadeo.imedicinfo.worker.MapDetailUpdateWorker;
import amadeo.imedicinfo.worker.PresentationRepositoryUpdateWorker;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationManagerCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "iMedicInfo";

    //region Constants
    public static final String ACTION_NAVIGATE = "amadeo.intent.action.NAVIGATE_TO";
    public static final String ARG_ACTION_NAVIGATE_DESTINATION = "destination";

    private static final String FIREBASE_MAIN_TOPIC = "imedicMainTopic";

    public static final String MAIN_NOTIFICATION_CHANNEL_ID = "mainChannel";
    public static final int MAIN_NOTIFICATION_ID = 0;

    //endregion

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavController navController;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBar);

        setContentView(R.layout.activity_main);

        setupNotifications();
        setupFCM();
        setupNavigation();
        setupWorkManager();

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        if (intent != null) {
            handleIntent(intent);
            setIntent(null);
        }
    }

    private void setupNotifications() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelName = getString(R.string.channel_main_name);
            String description = getString(R.string.channel_main_desc);

            NotificationChannel channel = new NotificationChannel(MAIN_NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(description);
            channel.enableLights(true);
            channel.setLightColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null)
                notificationManager.createNotificationChannel(channel);
        }
    }

    private void setupFCM() {
        FirebaseMessaging.getInstance().subscribeToTopic(FIREBASE_MAIN_TOPIC)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful())
                        Log.i(TAG, "topic subscription successful");
                    else
                        Log.w(TAG, "topic subscription unsuccessful!");
                });
    }

    private Set<Integer> getTopLevelDestinations(Menu menu) {
        Set<Integer> destinations = new HashSet<>();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.hasSubMenu()) {
                Set<Integer> subMenuDestinations = getTopLevelDestinations(item.getSubMenu());
                destinations.addAll(subMenuDestinations);
            } else {
                destinations.add(item.getItemId());
            }
        }

        return destinations;
    }

    private void setupNavigation() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(getTopLevelDestinations(navigationView.getMenu()))
                .setDrawerLayout(drawerLayout)
                .build();

        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        //TODO: find out what am I doing wrong such I have to update the title manually at the application launch
        Objects.requireNonNull(getSupportActionBar()).setTitle(Objects.requireNonNull(navController.getCurrentDestination()).getLabel());
    }

    private void setupWorkManager() {
        Constraints workerConstraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresBatteryNotLow(true)
                .build();

        PeriodicWorkRequest presentationCheckWork = new PeriodicWorkRequest.Builder(PresentationRepositoryUpdateWorker.class, 6, TimeUnit.HOURS)
                .setConstraints(workerConstraints)
                .build();
        WorkManager.getInstance().enqueueUniquePeriodicWork("periodic_presentation_update", ExistingPeriodicWorkPolicy.REPLACE, presentationCheckWork);

        PeriodicWorkRequest mapDetailCheckWork = new PeriodicWorkRequest.Builder(MapDetailUpdateWorker.class, 6, TimeUnit.HOURS)
                .setConstraints(workerConstraints)
                .build();
        WorkManager.getInstance().enqueueUniquePeriodicWork("periodic_map_detail_update", ExistingPeriodicWorkPolicy.REPLACE, mapDetailCheckWork);
    }

    private boolean handleIntent(Intent intent) {
        String action = intent.getAction();
        if (action == null)
            return false;

        Log.i(TAG, "new intent: " + action);
        switch (action) {
            case ACTION_NAVIGATE:
                NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
                notificationManagerCompat.cancel(MAIN_NOTIFICATION_ID);

                if (navigationView == null)
                    return false;

                Bundle extras = intent.getExtras();
                if (extras == null || !extras.containsKey(ARG_ACTION_NAVIGATE_DESTINATION))
                    return false;

                int destination = extras.getInt(ARG_ACTION_NAVIGATE_DESTINATION);
                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(destination, extras);

                return true;
            default:
                return false;
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        return navController.navigateUp();
    }
}