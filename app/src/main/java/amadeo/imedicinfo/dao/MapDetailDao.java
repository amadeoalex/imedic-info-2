package amadeo.imedicinfo.dao;

import amadeo.imedicinfo.entity.MapDetail;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface MapDetailDao {
    @Insert
    void insert(MapDetail mapDetail);

    @Update
    void update(MapDetail mapDetail);

    @Delete
    void delete(MapDetail mapDetail);

    @Query("DELETE FROM map_details_table")
    void deleteAllMapDetails();

    @Query("SELECT * FROM map_details_table WHERE id=:id")
    MapDetail getMapDetail(String id);
}
