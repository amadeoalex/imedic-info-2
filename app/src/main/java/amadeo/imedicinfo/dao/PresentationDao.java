package amadeo.imedicinfo.dao;

import java.util.List;

import amadeo.imedicinfo.entity.Presentation;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface PresentationDao {
    @Insert
    void insert(Presentation... presentations);

    @Update
    void update(Presentation... presentations);

    @Delete
    void delete(Presentation... presentations);

    @Query("DELETE FROM presentation_table WHERE id=:id")
    void delete(String id);

    @Query("DELETE FROM presentation_table")
    void deleteAllPresentations();

    @Query("SELECT * FROM presentation_table WHERE session=:session ORDER BY (presentationTime = '--:--') ASC, title ASC")
    LiveData<List<Presentation>> getPresentations(String session);

    @Query("SELECT * FROM presentation_table WHERE sessionType=:sessionType ORDER BY (presentationTime = '--:--') ASC, title ASC")
    LiveData<List<Presentation>> getPresentationsOfType(String sessionType);

    @Query("SELECT * FROM presentation_table ORDER BY (presentationTime = '--:--') ASC, title ASC")
    LiveData<List<Presentation>> getPresentations();

    @Query("SELECT id FROM presentation_table ORDER BY presentationTime DESC")
    List<String> getPresentationsIdList();

    @Query("SELECT * FROM presentation_table WHERE id=:id")
    Presentation getPresentation(String id);

    @Query("SELECT DISTINCT session FROM presentation_table ORDER BY session ASC")
    LiveData<List<String>> getSessions();

    @Query("SELECT DISTINCT session FROM presentation_table WHERE sessionType=:sessionType ORDER BY session ASC")
    LiveData<List<String>> getSessions(String sessionType);
}
