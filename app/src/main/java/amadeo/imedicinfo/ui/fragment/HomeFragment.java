package amadeo.imedicinfo.ui.fragment;


import android.os.Bundle;

import amadeo.imedicinfo.databinding.FragmentHomeBinding;
import amadeo.imedicinfo.ui.adapter.NewsAdapter;
import amadeo.imedicinfo.viewmodel.HomeViewModel;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import amadeo.imedicinfo.R;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class HomeFragment extends Fragment {

    public static final String ARG_ACTION_SHOW_DETAIL_TITLE = NewsDetailsFragment.ARG_TITLE;
    public static final String ARG_ACTION_SHOW_DETAIL_MESSAGE = NewsDetailsFragment.ARG_MESSAGE;

    private HomeViewModel homeViewModel;


    public HomeFragment() {
        // Required empty public constructor
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(requireActivity()).get(HomeViewModel.class);

        FragmentHomeBinding fragmentMapBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        fragmentMapBinding.setVmodel(homeViewModel);
        fragmentMapBinding.setLifecycleOwner(this);

        View root = fragmentMapBinding.getRoot();

        RecyclerView recyclerView = root.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        final NewsAdapter newsAdapter = new NewsAdapter();
        newsAdapter.setOnItemClickListener(news -> navigateToNewsDetail(news.getTitle(), news.getContent()));

        homeViewModel.news.observe(this, newsAdapter::submitList);
        recyclerView.setAdapter(newsAdapter);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(handleArguments(getArguments()))
            setArguments(null);
    }

    private boolean handleArguments(Bundle arguments) {
        if (arguments == null)
            return false;

        String title = arguments.getString(ARG_ACTION_SHOW_DETAIL_TITLE);
        String message = arguments.getString(ARG_ACTION_SHOW_DETAIL_MESSAGE);

        return navigateToNewsDetail(title, message);
    }

    private boolean navigateToNewsDetail(String title, String message) {
        if (title == null || message == null)
            return false;

        Bundle detailArguments = new Bundle();
        detailArguments.putString(NewsDetailsFragment.ARG_TITLE, title);
        detailArguments.putString(NewsDetailsFragment.ARG_MESSAGE, message);

        Navigation.findNavController(Objects.requireNonNull(getView())).navigate(R.id.action_nav_to_fragment_news_details, detailArguments);
        return true;
    }
}
