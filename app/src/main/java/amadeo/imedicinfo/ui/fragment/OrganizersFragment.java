package amadeo.imedicinfo.ui.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import amadeo.imedicinfo.R;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class OrganizersFragment extends Fragment {

    public OrganizersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_organizers, container, false);
    }

}
