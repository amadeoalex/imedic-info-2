package amadeo.imedicinfo.ui.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.integration.JavaScriptInterface;
import amadeo.imedicinfo.utils.InternetUtils;
import amadeo.imedicinfo.viewmodel.MapViewModel;
import amadeo.imedicinfo.worker.MapDetailUpdateWorker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

interface OnMapChangeListener {
    void mapChanged(String url);
}

public class MapFragment extends Fragment implements OnMapChangeListener {
    private final String KEY_URL = "url";

    private ConnectivityManager connectivityManager;
    private ConnectivityManager.NetworkCallback networkCallback;

    private String currentMap = MapInternalFragment.MAP_GROUND_URL;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();

        Activity activity = getActivity();
        if (activity == null)
            return;

        connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null)
            return;

        networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(@NonNull Network network) {
                refresh();
            }
        };

        NetworkRequest networkRequest = new NetworkRequest.Builder().build();
        connectivityManager.registerNetworkCallback(networkRequest, networkCallback);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (connectivityManager == null || networkCallback == null)
            return;

        connectivityManager.unregisterNetworkCallback(networkCallback);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_rules, container, false);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_URL))
                currentMap = savedInstanceState.getString(KEY_URL);
        }

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        refresh();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(KEY_URL, currentMap);
    }

    @Override
    public void mapChanged(String url) {
        currentMap = url;
    }

    private void refresh() {
        Fragment fragment = InternetUtils.isInternetAvailable(getActivity()) ? MapInternalFragment.newInstance(currentMap) : ErrorInfoFragment.newInstance(R.drawable.no_internet);
        if (fragment instanceof MapInternalFragment)
            ((MapInternalFragment) fragment).setListener(MapFragment.this);

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.nav_default_enter_anim, R.anim.nav_default_exit_anim);
        fragmentTransaction.replace(R.id.fragment_holder, fragment);
        fragmentTransaction.commit();
    }

    public static class MapInternalFragment extends Fragment {
        private static final String ARG_URL = "url";

        private static final String MAP_GROUND_URL = "https://imedicongress.cm.umk.pl/app/map/map-ground.html";
        private static final String MAP_FIRST_URL = "https://imedicongress.cm.umk.pl/app/map/map-first.html";
        private static final String MAP_SECOND_URL = "https://imedicongress.cm.umk.pl/app/map/map-second.html";

        private ProgressBar progressBar;
        private WebView webView;

        private OnMapChangeListener listener;

        public static MapInternalFragment newInstance(String url) {
            MapInternalFragment fragment = new MapInternalFragment();
            Bundle args = new Bundle();
            args.putString(ARG_URL, url);
            fragment.setArguments(args);
            return fragment;
        }

        public MapInternalFragment() {
            // Required empty public constructor
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        @SuppressLint("SetJavaScriptEnabled")
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.fragment_map_internal, container, false);

            progressBar = root.findViewById(R.id.rules_progressbar);

            webView = root.findViewById(R.id.rules_webview);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setDomStorageEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(false);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);

            webView.addJavascriptInterface(new JavaScriptInterface(getActivity(), ViewModelProviders.of(this).get(MapViewModel.class)), "jsProxy");

            webView.setWebChromeClient(new WebChromeClient() {
                @Override
                public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                    Log.i("iMedicWebView", consoleMessage.message());
                    return true;
                }
            });

            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    progressBar.setVisibility(View.GONE);
                    webView.setVisibility(View.VISIBLE);
                }
            });

            return root;
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            String url = getArguments() != null ? getArguments().getString(ARG_URL, MAP_GROUND_URL) : MAP_GROUND_URL;
            loadMap(url);
        }

        @Override
        public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
            inflater.inflate(R.menu.fragment_map_menu, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }

        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.map_ground:
                    loadMap(MAP_GROUND_URL);
                    return true;

                case R.id.map_first:
                    loadMap(MAP_FIRST_URL);
                    return true;

                case R.id.map_second:
                    loadMap(MAP_SECOND_URL);
                    return true;

                case R.id.update:
                    webView.clearCache(true);
                    webView.reload();
                    OneTimeWorkRequest updateMapDetailWork = new OneTimeWorkRequest.Builder(MapDetailUpdateWorker.class).build();
                    WorkManager.getInstance().enqueueUniqueWork("forced_map_detail_update", ExistingWorkPolicy.REPLACE, updateMapDetailWork);
                    return true;

                default:
                    return super.onOptionsItemSelected(item);
            }
        }

        public void setListener(OnMapChangeListener listener) {
            this.listener = listener;
        }

        private void loadMap(String url) {
            if (InternetUtils.isInternetAvailable(getActivity())) {
                webView.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                webView.setInitialScale(1);
                webView.loadUrl(url);

                if (listener != null)
                    listener.mapChanged(url);
            } else
                Snackbar.make(webView, R.string.enable_internet, Snackbar.LENGTH_LONG).show();
        }
    }
}
