package amadeo.imedicinfo.ui.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import amadeo.imedicinfo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsDetailsFragment extends Fragment {

    public static final String ARG_TITLE = "title";
    public static final String ARG_MESSAGE = "message";

    public NewsDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_news_details, container, false);

        Bundle arguments = getArguments();
        if (arguments == null)
            return root;

        String title = arguments.getString(ARG_TITLE, "unknown");
        String message = arguments.getString(ARG_MESSAGE, "unknown");

        TextView titleTextView = root.findViewById(R.id.title_text_view);
        titleTextView.setText(title);

        TextView messageTextView = root.findViewById(R.id.message_text_view);
        messageTextView.setText(message);

        return root;
    }

}
