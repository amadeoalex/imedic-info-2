package amadeo.imedicinfo.ui.fragment;


import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.utils.UIUtils;
import amadeo.imedicinfo.worker.FileUpdateWorker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.TextViewCompat;
import androidx.fragment.app.Fragment;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.google.android.material.snackbar.Snackbar;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

import static amadeo.imedicinfo.utils.AgendaUtils.*;

public class AgendaFragment extends Fragment {
    private static final String TAG = "iMedicInfo AF";

    public static final String FILE_URL = "https://imedicongress.cm.umk.pl/app/map/agenda.json";
    public static final String FILE_NAME = "agenda.json";
    public static final String FILE_HASH_KEY = "agendaJSONHash";

    private final int COLOR_MEDICAL = Color.parseColor("#d85a5c");
    private final int COLOR_PHARMACEUTICAL = Color.parseColor("#aad279");
    private final int COLOR_HEALTH = Color.parseColor("#5e9fd4");
    private final int COLOR_DEFAULT = Color.parseColor("#ffd966");

    private GridLayout gridLayout;
    private String message = null;

    public AgendaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_agenda, container, false);

        gridLayout = root.findViewById(R.id.grid_layout);
        populateGridLayout();

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (message != null) {
            Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
            message = null;
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_agenda_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.update) {
            Data data = new Data.Builder()
                    .putString(FileUpdateWorker.ARG_URL, FILE_URL)
                    .putString(FileUpdateWorker.ARG_FILE_NAME, FILE_NAME)
                    .putString(FileUpdateWorker.ARG_HASH_KEY, FILE_HASH_KEY)
                    .build();

            OneTimeWorkRequest updateWork = new OneTimeWorkRequest.Builder(FileUpdateWorker.class)
                    .setInputData(data)
                    .build();
            WorkManager.getInstance().enqueueUniqueWork("forced_agenda_update", ExistingWorkPolicy.REPLACE, updateWork);
            WorkManager.getInstance().getWorkInfoByIdLiveData(updateWork.getId())
                    .observe(AgendaFragment.this, workInfo -> {
                        if (workInfo != null && getView() != null) {
                            if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                                int result = workInfo.getOutputData().getInt(FileUpdateWorker.KEY_RESULT, -1);
                                if (result == FileUpdateWorker.RESULT_UP_TO_DATE) {
                                    Snackbar.make(getView(), R.string.update_uptodate, Snackbar.LENGTH_SHORT).show();
                                } else if (result == FileUpdateWorker.RESULT_UPDATED) {
                                    Snackbar.make(getView(), R.string.update_finished, Snackbar.LENGTH_SHORT).show();
                                    populateGridLayout();
                                }
                            } else if (workInfo.getState() == WorkInfo.State.FAILED)
                                Snackbar.make(getView(), R.string.update_failed, Snackbar.LENGTH_SHORT).show();
                        }
                    });

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private JSONObject getAgenda() throws JSONException {
        JSONObject agenda;

        Activity activity = getActivity();
        if (activity == null) {
            Log.i(TAG, "activity is null, generating agenda");
            agenda = createAgendaData();
        } else {
            try (FileInputStream fileInputStream = activity.openFileInput(FILE_NAME);
                 Scanner scanner = new Scanner(fileInputStream, "UTF-8").useDelimiter("\\A")) {

                String contents = scanner.next();
                agenda = new JSONObject(contents);
            } catch (IOException e) {
                Log.i(TAG, "error opening file, generating agenda");
                agenda = createAgendaData();
            }
        }

        return agenda;
    }

    private JSONObject createAgendaData() throws JSONException {
        JSONArray data = new JSONArray();
        data.put(new JSONArray().put(createCell("", 0, BORDER_BOTTOM | BORDER_RIGHT)).put(createCell("Lecture Hall A", 1, BORDER_RIGHT | BORDER_BOTTOM)).put(createCell("Lecture Hall B", 2, BORDER_RIGHT | BORDER_BOTTOM)).put(createCell("Lecture Hall C", 3, BORDER_RIGHT | BORDER_BOTTOM)).put(createCell("1st floor corridor", 4, BORDER_BOTTOM)));
        data.put(new JSONArray().put(createCell("8:00", 0, BORDER_RIGHT | BORDER_BOTTOM)).put(createCell("Inauguration Ceremony", 1, 4, 1, BORDER_BOTTOM, -1, Color.TRANSPARENT)));
        data.put(new JSONArray().put(createCell("8:30", 0, BORDER_RIGHT)).put(createCell("Health Sciences Block", 1, 1, 11, BORDER_RIGHT, -1, COLOR_HEALTH)).put(createCell("Medical Sciences Block", 2, 1, 11, BORDER_RIGHT, -1, COLOR_MEDICAL)).put(createCell("Pharmaceutical Sciences Block", 3, 1, 11, BORDER_RIGHT, -1, COLOR_PHARMACEUTICAL)).put(createCell("Poster Block", 4, 1, 11, COLOR_DEFAULT)));
        data.put(new JSONArray().put(createCell("9:00", 0, BORDER_RIGHT)));
        data.put(new JSONArray().put(createCell("10:00", 0, BORDER_RIGHT)));
        data.put(new JSONArray().put(createCell("11:00", 0, BORDER_RIGHT)));
        data.put(new JSONArray().put(createCell("12:00", 0, BORDER_RIGHT)));
        data.put(new JSONArray().put(createCell("13:00", 0, BORDER_RIGHT)));
        data.put(new JSONArray().put(createCell("14:00", 0, BORDER_RIGHT)));
        data.put(new JSONArray().put(createCell("15:00", 0, BORDER_RIGHT)));
        data.put(new JSONArray().put(createCell("16:00", 0, BORDER_RIGHT)));
        data.put(new JSONArray().put(createCell("17:00", 0, BORDER_RIGHT)));
        data.put(new JSONArray().put(createCell("18:00", 0, BORDER_RIGHT)));

        int colNum = 0;
        for (int i = 0; i < data.length(); i++) {
            int length = data.getJSONArray(i).length();
            if (length > colNum)
                colNum = length;
        }

        JSONObject agenda = new JSONObject();
        agenda.put(ROW_NUM, data.length());
        agenda.put(COL_NUM, colNum);

        agenda.put(CELL_DATA, data);

        JSONObject message = new JSONObject();
        message.put(MESSAGE_SHOW, true);
        message.put(MESSAGE_TEXT, "May (and will) change.");
        agenda.put(MESSAGE, message);

        return agenda;
    }

    private void populateGridLayout() {
        try {
            if (gridLayout == null)
                return;

            Activity activity = getActivity();
            if (activity != null) {
                JSONObject agenda = getAgenda();

                int rowNum = agenda.getInt(ROW_NUM);
                int colNum = agenda.getInt(COL_NUM);

                gridLayout.setRowCount(rowNum);
                gridLayout.setColumnCount(colNum);

                JSONArray rowData = agenda.getJSONArray(CELL_DATA);
                for (int rowIndex = 0; rowIndex < rowData.length(); rowIndex++) {
                    JSONArray row = rowData.getJSONArray(rowIndex);

                    for (int columnIndex = 0; columnIndex < row.length(); columnIndex++) {
                        JSONObject element = row.getJSONObject(columnIndex);
                        AppCompatTextView textView = new AppCompatTextView(activity);
                        textView.setText(element.getString(PROP_TEXT));
                        textView.setGravity(Gravity.CENTER);
                        int padding = UIUtils.toDP(activity, 4);
                        textView.setPadding(padding, padding, padding, padding);
                        textView.setMaxLines(2);
                        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(textView, 8, 14, 1, TypedValue.COMPLEX_UNIT_SP);

                        if (element.has(PROP_BORDER)) {
                            int borderEnabled = element.getInt(PROP_BORDER);
                            int left = (borderEnabled & 1) != 0 ? 2 : 0;
                            int top = (borderEnabled & 2) != 0 ? 2 : 0;
                            int right = (borderEnabled & 4) != 0 ? 2 : 0;
                            int bottom = (borderEnabled & 8) != 0 ? 2 : 0;

                            int borderColor = (element.has(PROP_BORDER_COLOR) && element.getInt(PROP_BORDER_COLOR) != -1) ? element.getInt(PROP_BORDER_COLOR) : getResources().getColor(R.color.colorPrimary);
                            int backgroundColor = element.has(PROP_BACKGROUND) ? element.getInt(PROP_BACKGROUND) : -1;
                            LayerDrawable border = getBorder(borderColor, backgroundColor, UIUtils.toDP(activity, left), UIUtils.toDP(activity, top), UIUtils.toDP(activity, right), UIUtils.toDP(activity, bottom));
                            textView.setBackground(border);
                        }

                        int rowSize = element.has(PROP_ROW_SPAN) ? element.getInt(PROP_ROW_SPAN) : 1;
                        int colSize = element.has(PROP_COL_SPAN) ? element.getInt(PROP_COL_SPAN) : 1;

                        float colWeight = columnIndex == 0 ? 0.7f : 1f;

                        GridLayout.LayoutParams params = new GridLayout.LayoutParams(GridLayout.spec(rowIndex, rowSize, 1f), GridLayout.spec(element.getInt(PROP_COL), colSize, colWeight));

//                    params.setGravity(Gravity.FILL_VERTICAL);
//                    params.width = 0;

                        params.width = 0;
                        params.height = 0;

                        textView.setLayoutParams(params);

                        gridLayout.addView(textView);
                    }
                }

                if (agenda.has(MESSAGE)) {
                    JSONObject messageData = agenda.getJSONObject(MESSAGE);
                    if (messageData.has(MESSAGE_SHOW) && messageData.getBoolean(MESSAGE_SHOW))
                        message = messageData.getString(MESSAGE_TEXT);
                }
            } else {
                Log.w(TAG, "activity is null");
            }
        } catch (JSONException e) {
            Log.w(TAG, "agenda json data is malformed");
        }
    }
}
