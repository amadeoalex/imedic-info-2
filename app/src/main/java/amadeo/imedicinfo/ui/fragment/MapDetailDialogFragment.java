package amadeo.imedicinfo.ui.fragment;

import android.app.Dialog;
import android.os.Bundle;

import amadeo.imedicinfo.R;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

public class MapDetailDialogFragment extends AppCompatDialogFragment {

    private static final String ARG_TITLE = "title";
    private static final String ARG_MESSAGE = "message";

    public MapDetailDialogFragment() {
        // Required empty public constructor
    }

    public static MapDetailDialogFragment newInstance(String title, String content) {
        MapDetailDialogFragment fragment = new MapDetailDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, content);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(requireActivity())
                .setTitle(getArgumentsString(ARG_TITLE))
                .setMessage(getArgumentsString(ARG_MESSAGE))
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    if (dialog != null)
                        dialog.dismiss();
                });

        return alertDialogBuilder.create();
    }

    private String getArgumentsString(String key) {
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(key))
            return arguments.getString(key);
        else
            return "UNKNOWN";
    }
}
