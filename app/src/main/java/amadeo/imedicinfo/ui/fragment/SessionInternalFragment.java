package amadeo.imedicinfo.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import amadeo.imedicinfo.MainActivity;
import amadeo.imedicinfo.R;
import amadeo.imedicinfo.databinding.FragmentSessionInternalBinding;
import amadeo.imedicinfo.entity.Presentation;
import amadeo.imedicinfo.ui.adapter.PresentationAdapter;
import amadeo.imedicinfo.viewmodel.SessionViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SessionInternalFragment extends Fragment {
    private final String TAG = "iMedicInfo SF";

    public static final String ARG_SESSION_TYPE = "sessionType";

    private SessionViewModel sessionViewModel;
    private PresentationAdapter presentationAdapter;
    private String sessionType = Presentation.TYPE_ALL;

    private final Animation dummyAnimation = new AlphaAnimation(1, 1);

    public SessionInternalFragment() {
        // Required empty public constructor
    }

    public static SessionInternalFragment newInstance(String sessionType) {
        SessionInternalFragment fragment = new SessionInternalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SESSION_TYPE, sessionType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dummyAnimation.setDuration(700);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(ARG_SESSION_TYPE))
            sessionType = arguments.getString(ARG_SESSION_TYPE);
        else
            Log.w(TAG, "missing session type argument, all presentations will be visible");

        sessionViewModel = ViewModelProviders.of(requireActivity()).get(SessionViewModel.class);

        FragmentSessionInternalBinding fragmentSessionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_session_internal, container, false);
        fragmentSessionBinding.setVmodel(sessionViewModel);
        fragmentSessionBinding.setLifecycleOwner(getActivity());

        View root = fragmentSessionBinding.getRoot();

        setupPresentations(root);
        setupSessionSpinner(root);

        return root;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (!enter && getParentFragment() != null) {
            return dummyAnimation;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    private void setupPresentations(View root) {
        RecyclerView recyclerView = root.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        presentationAdapter = new PresentationAdapter();
        presentationAdapter.setOnItemClickListener(presentation -> {
            Bundle detailArguments = new Bundle();
            detailArguments.putString(PresentationDetailsFragment.ARG_TITLE, presentation.getTitle());
            detailArguments.putString(PresentationDetailsFragment.ARG_AUTHOR, presentation.getAuthor());
            detailArguments.putString(PresentationDetailsFragment.ARG_ABSTRACT, presentation.getPresentationAbstract());

            Navigation.findNavController(Objects.requireNonNull(getView())).navigate(R.id.action_nav_to_fragment_presentation_details, detailArguments);
        });

        LiveData<List<Presentation>> presentations = !sessionType.equals(Presentation.TYPE_ALL) ? sessionViewModel.getPresentationsOfType(sessionType) : sessionViewModel.getPresentations();
        presentations.observe(this, presentations1 -> {
            if (presentations1 != null && presentations1.size() > 0)
                presentationAdapter.submitList(presentations1, false);
        });
        recyclerView.setAdapter(presentationAdapter);
    }

    private void setupSessionSpinner(View root) {
        LinearLayout spinnerContainer = root.findViewById(R.id.session_spinner_container);
        spinnerContainer.setVisibility(View.VISIBLE);

        AppCompatSpinner spinner = root.findViewById(R.id.session_spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedSession = parent.getItemAtPosition(position).toString();
                Log.i(TAG, "selected " + selectedSession);

                String filter = selectedSession.equals(getString(R.string.all)) ? "" : selectedSession;
                presentationAdapter.getFilter().filter(filter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                presentationAdapter.getFilter().filter(Presentation.TYPE_ALL);
            }
        });

        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null && mainActivity.getSupportActionBar() != null) {
            sessionViewModel.getSessions(sessionType).observe(this, strings -> {
                ArrayList<String> sessions = new ArrayList<>(strings);
                if (sessions.size() > 1)
                    sessions.add(0, getString(R.string.all));

                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(mainActivity.getSupportActionBar().getThemedContext(), R.layout.support_simple_spinner_dropdown_item, sessions);
                spinner.setAdapter(spinnerAdapter);
                spinner.setSelection(0);
            });
        }
    }
}
