package amadeo.imedicinfo.ui.fragment;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import amadeo.imedicinfo.R;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.snackbar.Snackbar;

public class ErrorInfoFragment extends Fragment {
    private static final String ARG_RESOURCE_ID = "resourceId";
    private static final String ARG_MESSAGE_ID = "messageId";

    private int resourceId;
    private boolean shownMessage;

    public ErrorInfoFragment() {
        // Required empty public constructor
    }

    public static ErrorInfoFragment newInstance(int resourceId, int messageId) {
        ErrorInfoFragment fragment = newInstance(resourceId);
        if (fragment.getArguments() != null)
            fragment.getArguments().putInt(ARG_MESSAGE_ID, messageId);

        return fragment;
    }

    public static ErrorInfoFragment newInstance(int resourceId) {
        ErrorInfoFragment fragment = new ErrorInfoFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_RESOURCE_ID, resourceId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null)
            resourceId = arguments.getInt(ARG_RESOURCE_ID);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_error_info, container, false);

        Activity activity = getActivity();
        if (activity != null) {
            ImageView imageView = root.findViewById(R.id.drawable_imageview);
            Drawable drawable = getResources().getDrawable(resourceId, activity.getTheme());
            imageView.setImageDrawable(drawable);
        }

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(shownMessage)
            return;

        Bundle arguments = getArguments();
        if (arguments == null || !arguments.containsKey(ARG_MESSAGE_ID))
            return;

        Snackbar.make(view, arguments.getInt(ARG_MESSAGE_ID), Snackbar.LENGTH_LONG).show();
        shownMessage = true;
    }
}
