package amadeo.imedicinfo.ui.fragment;


import android.os.Bundle;

import amadeo.imedicinfo.R;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PresentationDetailsFragment extends Fragment {

    public static final String ARG_TITLE = "title";
    public static final String ARG_AUTHOR = "author";
    public static final String ARG_ABSTRACT = "abstract";

    public PresentationDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_presentation_details, container, false);

        Bundle arguments = getArguments();
        if (arguments == null)
            return root;

        String title = arguments.getString(ARG_TITLE, "unknown");
        String author = arguments.getString(ARG_AUTHOR, "unknown");
        String presentationAbstract = arguments.getString(ARG_ABSTRACT, "unknown");

        TextView titleTextView = root.findViewById(R.id.title_text_view);
        titleTextView.setText(title);

        TextView authorTextView = root.findViewById(R.id.author_text_view);
        authorTextView.setText(author);

        TextView abstractTextView = root.findViewById(R.id.abstract_text_view);
        abstractTextView.setMovementMethod(new ScrollingMovementMethod());
        abstractTextView.setText(presentationAbstract);

        return root;
    }

}
