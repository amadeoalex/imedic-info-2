package amadeo.imedicinfo.ui.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import amadeo.imedicinfo.R;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

public class SettingsFragment extends PreferenceFragmentCompat {

    private static final String PRIVACY_URL = "https://imedicongress.cm.umk.pl/2019/index.php/imedic-info-privacy-policy/";


    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.main_preferences, rootKey);

        Preference privacyPolicy = findPreference("preference_privacy");
        if(privacyPolicy!=null)
            privacyPolicy.setOnPreferenceClickListener(preference -> {
                Intent privacyIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PRIVACY_URL));
                startActivity(privacyIntent);

                return true;
            });
    }
}

