package amadeo.imedicinfo.ui.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.entity.Presentation;
import amadeo.imedicinfo.ui.adapter.PresentationAdapter;
import amadeo.imedicinfo.viewmodel.SessionViewModel;
import amadeo.imedicinfo.worker.PresentationRepositoryUpdateWorker;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

public class SessionFragment extends Fragment {
    private final String TAG = "iMedicInfo SF";

    private SessionViewModel sessionViewModel;
    private PresentationAdapter presentationAdapter;
    private String sessionType = Presentation.TYPE_ALL;


    public SessionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(SessionInternalFragment.ARG_SESSION_TYPE))
            sessionType = arguments.getString(SessionInternalFragment.ARG_SESSION_TYPE);

        sessionViewModel = ViewModelProviders.of(requireActivity()).get(SessionViewModel.class);

        View root = inflater.inflate(R.layout.fragment_session, container, false);

        sessionViewModel.getPresentationsOfType(sessionType).observe(SessionFragment.this, presentationList -> {
            Fragment fragment = (presentationList != null && presentationList.size() > 0) ? SessionInternalFragment.newInstance(sessionType) : ErrorInfoFragment.newInstance(R.drawable.coming_soon, R.string.no_abstracts);

            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.nav_default_enter_anim, R.anim.nav_default_exit_anim);
            fragmentTransaction.replace(R.id.fragment_holder, fragment);
            fragmentTransaction.commit();
        });

        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_session_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.update) {
            OneTimeWorkRequest updateWork = new OneTimeWorkRequest.Builder(PresentationRepositoryUpdateWorker.class)
                    .build();

            WorkManager.getInstance().enqueueUniqueWork("forced_presentation_update", ExistingWorkPolicy.REPLACE, updateWork);
            WorkManager.getInstance().getWorkInfoByIdLiveData(updateWork.getId())
                    .observe(SessionFragment.this, workInfo -> {
                        if (workInfo != null && getView() != null) {
                            if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                                if (workInfo.getOutputData().getBoolean(PresentationRepositoryUpdateWorker.KEY_FILES_UP_TO_DATE, false))
                                    Snackbar.make(getView(), R.string.update_uptodate, Snackbar.LENGTH_SHORT).show();
                                else
                                    Snackbar.make(getView(), R.string.update_finished, Snackbar.LENGTH_SHORT).show();
                            } else if (workInfo.getState() == WorkInfo.State.FAILED)
                                Snackbar.make(getView(), R.string.update_failed, Snackbar.LENGTH_SHORT).show();
                        }
                    });
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
