package amadeo.imedicinfo.ui.fragment;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import amadeo.imedicinfo.MainActivity;
import amadeo.imedicinfo.repository.NewsRepository;
import amadeo.imedicinfo.repository.PresentationRepository;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;

import amadeo.imedicinfo.R;
import androidx.work.WorkManager;

import static androidx.core.app.NotificationCompat.DEFAULT_SOUND;
import static androidx.core.app.NotificationCompat.PRIORITY_DEFAULT;

/**
 * A simple {@link Fragment} subclass.
 */
public class DebugFragment extends Fragment {
    public DebugFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_debug, container, false);

        root.findViewById(R.id.debug_clear_works).setOnClickListener(v -> {
            WorkManager.getInstance().cancelAllWork();
            Snackbar.make(v, "Work cleared", Snackbar.LENGTH_SHORT).show();
        });

        root.findViewById(R.id.debug_clear_database).setOnClickListener(v -> {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            preferences.edit().putString("dataHash", "").apply();
            PresentationRepository presentationRepository = new PresentationRepository(requireActivity().getApplication());
            presentationRepository.deleteAllPresentations();
        });

        root.findViewById(R.id.debug_show_notif).setOnClickListener(v -> {
            Intent showIntent = new Intent(getActivity(), MainActivity.class);
            showIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            showIntent.setAction(MainActivity.ACTION_NAVIGATE);
            showIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            showIntent.putExtra(MainActivity.ARG_ACTION_NAVIGATE_DESTINATION, R.id.nav_home);
            showIntent.putExtra(HomeFragment.ARG_ACTION_SHOW_DETAIL_TITLE, "Debug Notification");
            showIntent.putExtra(HomeFragment.ARG_ACTION_SHOW_DETAIL_MESSAGE, "I like pancakes");

            PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 21, showIntent, PendingIntent.FLAG_ONE_SHOT);

            Notification notification = buildNotification(MainActivity.MAIN_NOTIFICATION_CHANNEL_ID, R.drawable.ic_notification, null, "Debug Notification", "I like pancakes", pendingIntent);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(requireActivity());
            notificationManager.notify(MainActivity.MAIN_NOTIFICATION_ID, notification);
        });

        root.findViewById(R.id.debug_clear_database_m).setOnClickListener(v -> {
            NewsRepository newsRepository = new NewsRepository(requireActivity().getApplication());
            newsRepository.deleteAllNews();
        });

        return root;
    }

    private Notification buildNotification(String channelId, int iconResource, Bitmap largeIcon, String title, String content, PendingIntent pendingIntent) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(requireActivity(), channelId)
                .setSmallIcon(iconResource)
                .setDefaults(DEFAULT_SOUND)
                .setPriority(PRIORITY_DEFAULT)
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(pendingIntent);

        if (largeIcon != null)
            builder.setColor(Color.TRANSPARENT)
                    .setLargeIcon(largeIcon);
        else
            builder.setColor(getResources().getColor(R.color.colorAccent));

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
            builder.setLights(getResources().getColor(R.color.colorAccent), 2000, 1000);

        return builder.build();
    }
}
