package amadeo.imedicinfo.ui.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.entity.Presentation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class PresentationAdapter extends ListAdapter<Presentation, PresentationAdapter.PresentationHolder> implements Filterable {

    private final int COLOR_MEDICAL = Color.parseColor("#ce3133");
    private final int COLOR_PHARMACEUTICAL = Color.parseColor("#9aca5f");
    private final int COLOR_HEALTH = Color.parseColor("#3b8bcb");
    private final int COLOR_DEFAULT = Color.parseColor("#ffbf00");

    public interface OnItemClickListener {
        void onItemClick(Presentation presentation);
    }

    private OnItemClickListener onItemClickListener;
    private List<Presentation> unfilteredList;

    public PresentationAdapter() {
        super(DIFF_CALLBACK);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public PresentationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_presentation, parent, false);

        return new PresentationHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PresentationHolder holder, int position) {
        Presentation presentation = getItem(position);
        holder.authorTextView.setText(presentation.getAuthor());
        holder.titleTextView.setText(presentation.getTitle());
        holder.timestampTextView.setText(presentation.getPresentationTime());
        holder.separator.setBackgroundColor(getPresentationColor(presentation));
    }

    private int getPresentationColor(Presentation presentation) {
        switch (presentation.getBlock().toLowerCase()) {
            case Presentation.BLOCK_MEDICAL:
                return COLOR_MEDICAL;
            case Presentation.BLOCK_PHARMACEUTICAL:
                return COLOR_PHARMACEUTICAL;
            case Presentation.BLOCK_HEALTH:
                return COLOR_HEALTH;
            default:
                return COLOR_DEFAULT;
        }
    }

    public void submitList(@Nullable List<Presentation> list, boolean filtered) {
        if (!filtered)
            unfilteredList = list;

        super.submitList(list);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Presentation> currentList = unfilteredList;
                List<Presentation> filteredList = new ArrayList<>();
                String sessionFilter = constraint.toString();

                if (sessionFilter.isEmpty()) {
                    filteredList = unfilteredList;
                } else {
                    for (Presentation presentation : currentList) {
                        if (presentation.getSession().equals(sessionFilter))
                            filteredList.add(presentation);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;

                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                List<Presentation> presentations = (List<Presentation>) results.values;
                submitList(presentations, true);
            }
        };
    }

    class PresentationHolder extends RecyclerView.ViewHolder {
        private final TextView authorTextView;
        private final TextView titleTextView;
        private final TextView timestampTextView;
        private final View separator;

        PresentationHolder(@NonNull View itemView) {
            super(itemView);

            authorTextView = itemView.findViewById(R.id.author_text_view);
            titleTextView = itemView.findViewById(R.id.title_text_view);
            timestampTextView = itemView.findViewById(R.id.timestamp_text_view);
            separator = itemView.findViewById(R.id.separator);

            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (onItemClickListener != null && position != RecyclerView.NO_POSITION) {
                    onItemClickListener.onItemClick(getItem(position));
                }
            });
        }
    }

    private static final DiffUtil.ItemCallback<Presentation> DIFF_CALLBACK = new DiffUtil.ItemCallback<Presentation>() {
        @Override
        public boolean areItemsTheSame(@NonNull Presentation oldItem, @NonNull Presentation newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Presentation oldItem, @NonNull Presentation newItem) {
            return oldItem.getAuthor().equals(newItem.getAuthor()) &&
                    oldItem.getPresentationAbstract().equals(newItem.getPresentationAbstract()) &&
                    oldItem.getPresentationTime().equals(newItem.getPresentationTime()) &&
                    oldItem.getTitle().equals(newItem.getTitle()) &&
                    oldItem.getSession().equals(newItem.getSession());
        }
    };
}
