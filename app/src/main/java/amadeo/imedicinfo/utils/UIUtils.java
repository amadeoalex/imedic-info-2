package amadeo.imedicinfo.utils;

import android.content.Context;
import android.util.Log;
import android.util.TypedValue;

import java.lang.reflect.Field;

public class UIUtils {
    private static final String TAG = "iMedicInfo UIUtils";

    public static int getResourceIdByName(String resourceName, Class<?> c) {
        try {
            Field field = c.getDeclaredField(resourceName);
            return field.getInt(field);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            Log.i(TAG, "cannot locate resource with name: " + resourceName);
            return -1;
        }
    }

    public static int toDP(Context context, int pixels) {
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pixels, context.getResources().getDisplayMetrics()) - 1f);
    }

}
