package amadeo.imedicinfo.utils;

import android.util.Base64;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SecurityUtils {

    public static String toBase64SHA256Hash(String string) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(string.getBytes(StandardCharsets.UTF_8));
        byte[] digest = messageDigest.digest();

        return Base64.encodeToString(digest, Base64.DEFAULT).trim();
    }

}
