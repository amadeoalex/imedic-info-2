package amadeo.imedicinfo.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateFormatter {
    private static final Calendar calendarNow = Calendar.getInstance();
    private static final Calendar calendarThen = Calendar.getInstance();

    private static Locale currentLocale;
    private static SimpleDateFormat dayDateFormat;
    private static SimpleDateFormat monthDateFormat;
    private static SimpleDateFormat fullDateFormat;

    private static void checkDateFormats() {
        if (currentLocale!= null && currentLocale.equals(Locale.getDefault()))
            return;

        currentLocale = Locale.getDefault();

        dayDateFormat = new SimpleDateFormat("HH:mm", currentLocale);
        monthDateFormat = new SimpleDateFormat("dd-MM HH:mm", currentLocale);
        fullDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", currentLocale);
    }

    private static boolean dayEquals(Calendar c1, Calendar c2) {
        return c1.get(Calendar.DATE) == c2.get(Calendar.DATE);
    }

    private static boolean monthEquals(Calendar c1, Calendar c2) {
        return c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH);
    }

    private static boolean yearEquals(Calendar c1, Calendar c2) {
        return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR);
    }

    public static String getConvenientDate(long timestamp) {
        checkDateFormats();

        calendarNow.setTimeInMillis(System.currentTimeMillis());
        calendarThen.setTimeInMillis(timestamp);

        if (dayEquals(calendarNow, calendarThen) && monthEquals(calendarNow, calendarThen) && yearEquals(calendarNow, calendarThen)) {
            return dayDateFormat.format(calendarThen.getTime());
        } else if (yearEquals(calendarNow, calendarThen) && monthEquals(calendarNow, calendarThen)) {
            return monthDateFormat.format(calendarThen.getTime());
        } else {
            return fullDateFormat.format(calendarThen.getTime());
        }
    }
}
