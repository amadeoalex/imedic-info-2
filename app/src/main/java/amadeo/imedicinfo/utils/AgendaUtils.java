package amadeo.imedicinfo.utils;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class AgendaUtils {

    public static final String COL_NUM = "colNum";
    public static final String ROW_NUM = "rowNum";
    public static final String CELL_DATA = "data";

    public static final String MESSAGE = "message";
    public static final String MESSAGE_SHOW = "show";
    public static final String MESSAGE_TEXT = "text";

    public static final String PROP_TEXT = "text";
    public static final String PROP_COL = "col";
    public static final String PROP_COL_SPAN = "colSpan";
    public static final String PROP_ROW_SPAN = "rowSpan";
    public static final String PROP_BORDER = "border";
    public static final String PROP_BORDER_COLOR = "bColor";
    public static final String PROP_BACKGROUND = "background";

    public static final int BORDER_LEFT = 1;
    public static final int BORDER_TOP = 2;
    public static final int BORDER_RIGHT = 4;
    public static final int BORDER_BOTTOM = 8;

    public static LayerDrawable getBorder(int borderColor, int backgroundColor, int dpLeft, int dpTop, int dpRight, int dpBottom) {
        int maxSize = Collections.max(Arrays.asList(dpLeft, dpTop, dpRight, dpBottom));

        GradientDrawable border = new GradientDrawable();
        border.setStroke(maxSize, borderColor);
        border.setGradientType(GradientDrawable.SWEEP_GRADIENT);

        ArrayList<Drawable> layers = new ArrayList<>();

        ColorDrawable colorDrawable = new ColorDrawable(backgroundColor == -1 ? Color.TRANSPARENT : backgroundColor);
        layers.add(colorDrawable);
        layers.add(border);

        LayerDrawable layerDrawable = new LayerDrawable(layers.toArray(new Drawable[0]));
        layerDrawable.setLayerInset(1, dpLeft - maxSize, dpTop - maxSize, dpRight - maxSize, dpBottom - maxSize);

        return layerDrawable;
    }

    public static JSONObject createCell(String text, int col, int colSpan, int rowSpan, int border, int borderColor, int backgroundColor) throws JSONException {
        return new JSONObject()
                .put(PROP_TEXT, text)
                .put(PROP_COL, col)
                .put(PROP_COL_SPAN, colSpan)
                .put(PROP_ROW_SPAN, rowSpan)
                .put(PROP_BORDER, border)
                .put(PROP_BORDER_COLOR, borderColor)
                .put(PROP_BACKGROUND, backgroundColor);
    }

    public static JSONObject createCell(String text, int col, int colSpan, int rowSpan, int backgroundColor) throws JSONException {
        return new JSONObject()
                .put(PROP_TEXT, text)
                .put(PROP_COL, col)
                .put(PROP_COL_SPAN, colSpan)
                .put(PROP_ROW_SPAN, rowSpan)
                .put(PROP_BORDER, 0)
                .put(PROP_BACKGROUND, backgroundColor);
    }


    public static JSONObject createCell(String text, int col, int border) throws JSONException {
        return new JSONObject()
                .put(PROP_TEXT, text)
                .put(PROP_COL, col)
                .put(PROP_COL_SPAN, 1)
                .put(PROP_ROW_SPAN, 1)
                .put(PROP_BORDER, border);
    }

    public static JSONObject createCell(String text, int col) throws JSONException {
        return new JSONObject()
                .put(PROP_TEXT, text)
                .put(PROP_COL, col)
                .put(PROP_COL_SPAN, 1)
                .put(PROP_ROW_SPAN, 1);
    }
}
