package amadeo.imedicinfo.datebase;

import android.content.Context;

import amadeo.imedicinfo.dao.MapDetailDao;
import amadeo.imedicinfo.entity.MapDetail;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {MapDetail.class}, version = 1, exportSchema = false)
public abstract class MapDetailDatabase extends RoomDatabase {
    private static MapDetailDatabase instance;

    public abstract MapDetailDao mapDetailDao();

    public static MapDetailDatabase getInstance(final Context context) {
        if (instance == null) {
            synchronized (MapDetailDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(), MapDetailDatabase.class, "map_detail_database")
                            .fallbackToDestructiveMigration()
                            //.addCallback(roomCallback)
                            .build();
                }
            }
        }
        return instance;
    }
}
