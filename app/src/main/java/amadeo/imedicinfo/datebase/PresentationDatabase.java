package amadeo.imedicinfo.datebase;

import android.content.Context;

import amadeo.imedicinfo.dao.PresentationDao;
import amadeo.imedicinfo.entity.Presentation;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Presentation.class}, version = 1, exportSchema = false)
public abstract class PresentationDatabase extends RoomDatabase {
    private static PresentationDatabase instance;

    public abstract PresentationDao presentationDao();

    public static PresentationDatabase getInstance(final Context context) {
        if (instance == null) {
            synchronized (PresentationDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(), PresentationDatabase.class, "presentation_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return instance;
    }
}
