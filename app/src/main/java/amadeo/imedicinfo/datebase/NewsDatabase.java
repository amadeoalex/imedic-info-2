package amadeo.imedicinfo.datebase;

import android.content.Context;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.dao.NewsDao;
import amadeo.imedicinfo.entity.News;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {News.class}, version = 1, exportSchema = false)
public abstract class NewsDatabase extends RoomDatabase {
    private static NewsDatabase instance;

    public abstract NewsDao newsDao();

    public static NewsDatabase getInstance(final Context context) {
        if (instance == null) {
            synchronized (NewsDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(), NewsDatabase.class, "news_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(new Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    new CreateEntryDataAsyncTask(context, instance).execute();
                                }
                            })
                            .build();
                }
            }
        }
        return instance;
    }

    private static class CreateEntryDataAsyncTask extends AsyncTask<Void, Void, Void> {
        private final WeakReference<Context> contextWeakReference;
        private final NewsDao newsDao;

        CreateEntryDataAsyncTask(Context context, NewsDatabase newsDatabase) {
            this.contextWeakReference = new WeakReference<>(context);
            this.newsDao = newsDatabase.newsDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Context context = contextWeakReference.get();

            newsDao.insert(new News(context.getString(R.string.welcome), context.getString(R.string.welcome_message), System.currentTimeMillis()));
            newsDao.insert(new News(context.getString(R.string.thank_you), context.getString(R.string.thank_you_message), System.currentTimeMillis()));
            return null;
        }
    }
}
