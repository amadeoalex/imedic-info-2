package amadeo.imedicinfo;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import amadeo.imedicinfo.entity.News;
import amadeo.imedicinfo.repository.NewsRepository;
import amadeo.imedicinfo.ui.fragment.AgendaFragment;
import amadeo.imedicinfo.ui.fragment.HomeFragment;
import amadeo.imedicinfo.utils.UIUtils;
import amadeo.imedicinfo.worker.FileUpdateWorker;
import amadeo.imedicinfo.worker.MapDetailUpdateWorker;
import amadeo.imedicinfo.worker.PresentationRepositoryUpdateWorker;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import static androidx.core.app.NotificationCompat.DEFAULT_SOUND;
import static androidx.core.app.NotificationCompat.PRIORITY_DEFAULT;

public class FCMService extends FirebaseMessagingService {
    private static final String TAG = "iMedicInfo FCMS";

    private static final String KEY_NOTIFICATION_CHANNEL_ID = "notificationChannel";

    public static final String KEY_NOTIFICATION_TYPE = "notificationType";
    public static final String NOTIFICATION_TYPE_NOTIFICATION = "notification";
    public static final String NOTIFICATION_TYPE_MAINTENANCE = "maintenance";

    private static final String KEY_NOTIFICATION_TITLE = "notificationTitle";
    private static final String KEY_NOTIFICATION_CONTENT = "notificationContent";
    private static final String KEY_NOTIFICATION_ICON_RESOURCE_NAME = "iconResourceName";

    public static final String KEY_MAINTENANCE_ACTION = "maintenanceAction";
    public static final String MAINTENANCE_ACTION_UPDATE = "update";

    private NewsRepository newsRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        newsRepository = new NewsRepository(getApplication());
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.i(MainActivity.TAG, "received remote message");

        Map<String, String> data = remoteMessage.getData();
        if (data == null) {
            Log.i(TAG, "message is missing data");
            return;
        }

        String messageType = data.get(KEY_NOTIFICATION_TYPE);
        if (messageType == null) {
            Log.i(TAG, "message is missing type");
            return;
        }

        switch (messageType) {
            case NOTIFICATION_TYPE_NOTIFICATION:
                handleNotificationMessage(remoteMessage, data);
                break;

            case NOTIFICATION_TYPE_MAINTENANCE:
                handleMaintenanceMessage(remoteMessage, data);
                break;

            default:
                Log.i(TAG, "unknown message type: " + messageType);
                break;
        }
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.i(MainActivity.TAG, "new token acquired:" + s);
    }

    private void handleNotificationMessage(RemoteMessage remoteMessage, Map<String, String> data) {
        if ((!data.containsKey(KEY_NOTIFICATION_TITLE) || !data.containsKey(KEY_NOTIFICATION_CONTENT))) {
            Log.w(TAG, "notification message missing title or content");
            return;
        }

        newsRepository.insert(new News(data.get(KEY_NOTIFICATION_TITLE), data.get(KEY_NOTIFICATION_CONTENT), remoteMessage.getSentTime()));

        String channelId = data.containsKey(KEY_NOTIFICATION_CHANNEL_ID) ? data.get(KEY_NOTIFICATION_CHANNEL_ID) : MainActivity.MAIN_NOTIFICATION_CHANNEL_ID;

        int iconResource = getIconResource(data);

        PendingIntent pendingIntent = buildPendingIntent(data);

        Notification notification = buildNotification(channelId, iconResource, data.get(KEY_NOTIFICATION_TITLE), data.get(KEY_NOTIFICATION_CONTENT), pendingIntent);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
        notificationManager.notify(MainActivity.MAIN_NOTIFICATION_ID, notification);
    }

    private void handleMaintenanceMessage(RemoteMessage message, Map<String, String> data) {
        if (!data.containsKey(KEY_MAINTENANCE_ACTION)) {
            Log.w(TAG, "maintenance message is missing action");
            return;
        }


        String action = data.get(KEY_MAINTENANCE_ACTION);
        if (action == null) {
            Log.i(TAG, "maintenance action is null");
            return;
        }

        switch (action) {
            case MAINTENANCE_ACTION_UPDATE:
                forceUpdates();
                break;

            default:
                Log.i(TAG, "unknown maintenance action: " + action);
        }
    }

    private void forceUpdates() {
        Constraints workerConstraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresBatteryNotLow(true)
                .build();

        Data agendaData = new Data.Builder()
                .putString(FileUpdateWorker.ARG_URL, AgendaFragment.FILE_URL)
                .putString(FileUpdateWorker.ARG_FILE_NAME, AgendaFragment.FILE_NAME)
                .putString(FileUpdateWorker.ARG_HASH_KEY, AgendaFragment.FILE_HASH_KEY)
                .build();

        OneTimeWorkRequest updateAgendaWork = new OneTimeWorkRequest.Builder(FileUpdateWorker.class)
                .setConstraints(workerConstraints)
                .setInputData(agendaData)
                .build();
        WorkManager.getInstance().enqueueUniqueWork("forced_agenda_update", ExistingWorkPolicy.REPLACE, updateAgendaWork);

        OneTimeWorkRequest updateMapDetailWork = new OneTimeWorkRequest.Builder(MapDetailUpdateWorker.class)
                .setConstraints(workerConstraints)
                .setInputData(agendaData)
                .build();
        WorkManager.getInstance().enqueueUniqueWork("forced_map_detail_update", ExistingWorkPolicy.REPLACE, updateMapDetailWork);

        OneTimeWorkRequest updateWork = new OneTimeWorkRequest.Builder(PresentationRepositoryUpdateWorker.class)
                .setConstraints(workerConstraints)
                .build();
        WorkManager.getInstance().enqueueUniqueWork("forced_presentation_update", ExistingWorkPolicy.REPLACE, updateWork);
    }

    private int getIconResource(Map<String, String> data) {
        int iconResource = R.drawable.ic_notification;
        if (data.containsKey(KEY_NOTIFICATION_ICON_RESOURCE_NAME)) {
            int requestedResourceId = UIUtils.getResourceIdByName(data.get(KEY_NOTIFICATION_ICON_RESOURCE_NAME), R.drawable.class);
            if (requestedResourceId != -1)
                iconResource = requestedResourceId;
        }

        return iconResource;
    }

    private PendingIntent buildPendingIntent(Map<String, String> data) {
        Intent showIntent = new Intent(getApplicationContext(), MainActivity.class);
        showIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        showIntent.setAction(MainActivity.ACTION_NAVIGATE);
        showIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        showIntent.putExtra(MainActivity.ARG_ACTION_NAVIGATE_DESTINATION, R.id.nav_home);
        showIntent.putExtra(HomeFragment.ARG_ACTION_SHOW_DETAIL_TITLE, data.get(KEY_NOTIFICATION_TITLE));
        showIntent.putExtra(HomeFragment.ARG_ACTION_SHOW_DETAIL_MESSAGE, data.get(KEY_NOTIFICATION_CONTENT));

        return PendingIntent.getActivity(getApplicationContext(), 21, showIntent, PendingIntent.FLAG_ONE_SHOT);
    }

    private Notification buildNotification(String channelId, int iconResource, String title, String content, PendingIntent pendingIntent) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setSmallIcon(iconResource)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setDefaults(DEFAULT_SOUND)
                .setPriority(PRIORITY_DEFAULT)
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
            builder.setLights(getResources().getColor(R.color.colorPrimaryDark), 2000, 1000);

        return builder.build();
    }
}
