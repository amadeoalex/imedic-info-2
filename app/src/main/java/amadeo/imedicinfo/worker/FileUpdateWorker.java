package amadeo.imedicinfo.worker;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import amadeo.imedicinfo.utils.InternetUtils;
import amadeo.imedicinfo.utils.SecurityUtils;

public class FileUpdateWorker extends Worker {
    private final String TAG = "iMedicInfo FileUWorker";

    public static final String ARG_URL = "url";
    public static final String ARG_FILE_NAME = "fileName";
    public static final String ARG_HASH_KEY = "hashKey";

    public static final String KEY_RESULT = "result";
    public static final int RESULT_UPDATED = 1;
    public static final int RESULT_UP_TO_DATE = 0;

    public FileUpdateWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Data parameters = getInputData();

        String urlString = parameters.getString(ARG_URL);
        String fileName = parameters.getString(ARG_FILE_NAME);
        String hashKey = parameters.getString(ARG_HASH_KEY);

        if (urlString == null || fileName == null || hashKey == null) {
            Log.i(TAG, "missing input parameters");
            return Result.failure();
        }

        Log.i(TAG, "updating '" + fileName + "'");

        if (!InternetUtils.isInternetAvailable(getApplicationContext())) {
            Log.i(TAG, "update aborted, no internet access");
            return Result.failure();
        }

        try {
            URL url = new URL(urlString);

            try (BufferedInputStream bufferedInputStream = new BufferedInputStream(url.openStream());
                 Scanner scanner = new Scanner(bufferedInputStream, "UTF-8").useDelimiter("\\A")) {

                Data.Builder returnDataBuilder = new Data.Builder();
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                String contents = scanner.next();
                String contentsHash = SecurityUtils.toBase64SHA256Hash(contents);

                String oldHash = sharedPreferences.getString(hashKey, "");
                if (!contentsHash.equals(oldHash)) {
                    try (FileOutputStream fileOutputStream = getApplicationContext().openFileOutput(fileName, Context.MODE_PRIVATE)) {
                        fileOutputStream.write(contents.getBytes());
                    }

                    sharedPreferences.edit().putString(hashKey, contentsHash).apply();
                    returnDataBuilder.putInt(KEY_RESULT, RESULT_UPDATED);
                } else {
                    returnDataBuilder.putInt(KEY_RESULT, RESULT_UP_TO_DATE);
                }

                return Result.success(returnDataBuilder.build());
            } catch (IOException | NoSuchAlgorithmException e) {
                Log.w(TAG, "update failed: " + e.getMessage());
                return Result.failure();
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "malformed url " + urlString);
            return Result.failure();
        }
    }
}
