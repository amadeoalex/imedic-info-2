package amadeo.imedicinfo.worker;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import amadeo.imedicinfo.dao.PresentationDao;
import amadeo.imedicinfo.datebase.PresentationDatabase;
import amadeo.imedicinfo.entity.Presentation;
import amadeo.imedicinfo.utils.InternetUtils;
import amadeo.imedicinfo.utils.SecurityUtils;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class PresentationRepositoryUpdateWorker extends Worker {
    private final String TAG = "iMedicInfo PRepUpdate";

    //region Constants
    private final String COLUMN_NAME_ID = "Id_OPT_";
    private final String COLUMN_NAME_AUTHOR = "Presenting author";
    private final String COLUMN_NAME_PARTICIPATION_STATUS = "Participation status";
    private final String COLUMN_NAME_SESSION_TYPE = "Session type";
    private final String COLUMN_NAME_SESSION = "Field";
    private final String COLUMN_NAME_TITLE = "Title";
    private final String COLUMN_NAME_ABSTRACT = "Abstract";
    private final String COLUMN_NAME_BLOCK = "Block";
    private final String COLUMN_NAME_PRESENTATION_TIME = "Presentation time_OPT_";

    private final ArrayList<String> COLUMN_NAMES = new ArrayList<String>() {{
        add(COLUMN_NAME_ID);
        add(COLUMN_NAME_AUTHOR);
        add(COLUMN_NAME_PARTICIPATION_STATUS);
        add(COLUMN_NAME_SESSION_TYPE);
        add(COLUMN_NAME_SESSION);
        add(COLUMN_NAME_TITLE);
        add(COLUMN_NAME_ABSTRACT);
        add(COLUMN_NAME_BLOCK);
        add(COLUMN_NAME_PRESENTATION_TIME);
    }};

    private final String KEY_DATA_HASH = "dataHash";

    public final static String KEY_FILES_UP_TO_DATE = "upToDate";

    private final String PRESENTATION_DATA_HASH_URL = "https://imedicongress.cm.umk.pl/app/presentations.php?info=hash";
    private final String PRESENTATION_DATA_URL = "https://imedicongress.cm.umk.pl/app/presentations.php?id=all";
    //endregion

    private final PresentationDao presentationDao;

    private final SharedPreferences sharedPreferences;
    private final String dataHash;

    private final HashMap<String, Integer> columnIndexes;

    public PresentationRepositoryUpdateWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

        PresentationDatabase presentationDatabase = PresentationDatabase.getInstance(context.getApplicationContext());
        presentationDao = presentationDatabase.presentationDao();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        dataHash = sharedPreferences.getString(KEY_DATA_HASH, "");

        columnIndexes = new HashMap<>();
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.i(TAG, "updating presentation repository");
        if (!InternetUtils.isInternetAvailable(getApplicationContext())) {
            Log.i(TAG, "update aborted, no internet access");
            return Result.failure();
        }

        Data.Builder returnDataBuilder = new Data.Builder();

        try {
            if (!isDatabaseUpToDate()) {
                if (!updateDatabase())
                    return Result.failure();
            } else
                returnDataBuilder.putBoolean(KEY_FILES_UP_TO_DATE, true);

            return Result.success(returnDataBuilder.build());
        } catch (IOException | NoSuchAlgorithmException e) {
            Log.w(TAG, "update failed: " + e.getMessage());
            return Result.failure();
        } catch (JSONException e) {
            Log.w(TAG, "received json data is malformed: " + e.getMessage());
            return Result.failure();
        }
    }

    private boolean isDatabaseUpToDate() throws IOException, JSONException {
        URL presentationDataHashURL = new URL(PRESENTATION_DATA_HASH_URL);
        try (BufferedInputStream dataBufferedInputStream = new BufferedInputStream(presentationDataHashURL.openStream());
             Scanner dataScanner = new Scanner(dataBufferedInputStream, "UTF-8").useDelimiter("\\A")
        ) {
            String jsonString = dataScanner.next();
            JSONObject hashData = new JSONObject(jsonString);

            return hashData.getString("hash").equals(dataHash);
        }
    }

    private boolean updateDatabase() throws IOException, JSONException, NoSuchAlgorithmException {
        URL presentationDataURL = new URL(PRESENTATION_DATA_URL);

        try (BufferedInputStream dataBufferedInputStream = new BufferedInputStream(presentationDataURL.openStream());
             Scanner dataScanner = new Scanner(dataBufferedInputStream, "UTF-8").useDelimiter("\\A")
        ) {
            String jsonString = dataScanner.next();

            JSONObject data = new JSONObject(jsonString);
            if (data.getString("response").equals("error"))
                return false;

            JSONArray presentationData = data.getJSONArray("data");

            String newDataHash = SecurityUtils.toBase64SHA256Hash(presentationData.toString());

            if (presentationData.length() > 1) {
                findColumnIndexes(presentationData);
                updateData(presentationData, newDataHash);
            } else {
                Log.i(TAG, "no presentations available");
            }
        }

        return true;
    }

    private void findColumnIndexes(JSONArray presentationData) throws JSONException {
        JSONArray indexesArray = presentationData.getJSONArray(0);
        for (int i = 0; i < indexesArray.length(); i++) {
            String columnName = indexesArray.getString(i);
            columnIndexes.put(columnName, i);
        }

        for (String columnName : COLUMN_NAMES) {
            if (!columnIndexes.containsKey(columnName))
                throw new JSONException("provided json data is missing at least one column name: " + columnName);
        }
    }

    private int getColumnIndex(String columnName) {
        Integer index = columnIndexes.get(columnName);
        if (index == null)
            throw new IllegalArgumentException("there is no known index of " + columnName);

        return index;
    }

    private void updateData(JSONArray presentationData, String newHash) throws JSONException {
        List<String> removedPresentations = presentationDao.getPresentationsIdList();

        List<Presentation> insertList = new ArrayList<>();
        List<Presentation> updateList = new ArrayList<>();

        for (int i = 0; i < presentationData.length(); i++) {
            JSONArray presentationJSON = presentationData.getJSONArray(i);

            String participationType = presentationJSON.getString(getColumnIndex(COLUMN_NAME_PARTICIPATION_STATUS));
            if (!participationType.equals("Active participant"))
                continue;

            String id = presentationJSON.getString(getColumnIndex(COLUMN_NAME_ID)).trim();
            String sessionType = presentationJSON.getString(getColumnIndex(COLUMN_NAME_SESSION_TYPE)).trim();
            String session = presentationJSON.getString(getColumnIndex(COLUMN_NAME_SESSION)).trim();
            String title = presentationJSON.getString(getColumnIndex(COLUMN_NAME_TITLE)).trim();
            String author = presentationJSON.getString(getColumnIndex(COLUMN_NAME_AUTHOR)).trim();
            String presentationAbstract = presentationJSON.getString(getColumnIndex(COLUMN_NAME_ABSTRACT)).trim();
            String block = presentationJSON.getString(getColumnIndex(COLUMN_NAME_BLOCK)).trim();
            String presentationTime = presentationJSON.getString(getColumnIndex(COLUMN_NAME_PRESENTATION_TIME)).trim();

            Presentation presentation = presentationDao.getPresentation(id);
            if (presentation != null) {
                if (!presentation.getSession().equals(session) ||
                        !presentation.getTitle().equals(title) ||
                        !presentation.getAuthor().equals(author) ||
                        !presentation.getPresentationAbstract().equals(presentationAbstract) ||
                        !presentation.getBlock().equals(block) ||
                        !presentation.getPresentationTime().equals(presentationTime)) {
                    presentation.setSession(session);
                    presentation.setTitle(title);
                    presentation.setAuthor(author);
                    presentation.setPresentationAbstract(presentationAbstract);
                    presentation.setBlock(block);
                    presentation.setPresentationTime(presentationTime);
                    updateList.add(presentation);
                }
            } else {
                presentation = new Presentation(id, session, sessionType, title, author, presentationAbstract, "--:--", block);
                insertList.add(presentation);
            }

            removedPresentations.remove(presentation.getId());
        }

        presentationDao.insert(insertList.toArray(new Presentation[0]));
        presentationDao.update(updateList.toArray(new Presentation[0]));

        for (String presentationId : removedPresentations)
            presentationDao.delete(presentationId);

        sharedPreferences.edit().putString(KEY_DATA_HASH, newHash).apply();
    }
}
