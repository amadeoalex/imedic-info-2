package amadeo.imedicinfo.worker;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import amadeo.imedicinfo.dao.MapDetailDao;
import amadeo.imedicinfo.datebase.MapDetailDatabase;
import amadeo.imedicinfo.entity.MapDetail;

import amadeo.imedicinfo.utils.InternetUtils;
import amadeo.imedicinfo.utils.SecurityUtils;
import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class MapDetailUpdateWorker extends Worker {
    private final String TAG = "iMedicInfo MapDUpdate";

    //region Constants
    private final String COLUMN_NAME_ID = "Id";
    private final String COLUMN_NAME_TITLE = "Title";
    private final String COLUMN_NAME_MESSAGE = "Message";


    private final ArrayList<String> COLUMN_NAMES = new ArrayList<String>() {{
        add(COLUMN_NAME_ID);
        add(COLUMN_NAME_TITLE);
        add(COLUMN_NAME_MESSAGE);
    }};

    private final String KEY_DATA_HASH = "mapDetailDataHash";

    private final static String KEY_FILES_UP_TO_DATE = "upToDate";

    private final String MAP_DETAIL_DATA_URL = "https://imedicongress.cm.umk.pl/app/map.php?id=all";
    //endregion

    private final MapDetailDao mapDetailDao;

    private final SharedPreferences sharedPreferences;
    private final String dataHash;

    private final HashMap<String, Integer> columnIndexes;

    public MapDetailUpdateWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

        MapDetailDatabase mapDetailDatabase = MapDetailDatabase.getInstance(context.getApplicationContext());
        mapDetailDao = mapDetailDatabase.mapDetailDao();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        dataHash = sharedPreferences.getString(KEY_DATA_HASH, "");

        columnIndexes = new HashMap<>();
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.i(TAG, "updating map detail database");
        if(!InternetUtils.isInternetAvailable(getApplicationContext())){
            Log.i(TAG, "update aborted, no internet access");
            return Result.failure();
        }

        try {
            URL mapDetailDataURL = new URL(MAP_DETAIL_DATA_URL);

            try (BufferedInputStream dataBufferedInputStream = new BufferedInputStream(mapDetailDataURL.openStream());
                 Scanner dataScanner = new Scanner(dataBufferedInputStream, "UTF-8").useDelimiter("\\A")
            ) {

                String jsonString = dataScanner.next();
                String newDataHash = SecurityUtils.toBase64SHA256Hash(jsonString);

                JSONObject data = new JSONObject(jsonString);
                JSONArray mapDetailData = data.getJSONArray("data");

                Data.Builder returnDataBuilder = new Data.Builder();

                findColumnIndexes(mapDetailData);
                if (!updateData(mapDetailData, newDataHash))
                    returnDataBuilder.putBoolean(KEY_FILES_UP_TO_DATE, true);

                return Result.success(returnDataBuilder.build());
            } catch (IOException | NoSuchAlgorithmException e) {
                Log.w(TAG, "update failed: " + e.getMessage());
                return Result.failure();
            } catch (JSONException e) {
                Log.w(TAG, "received json data is malformed: " + e.getMessage());
                return Result.failure();
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "malformed url");
            return Result.failure();
        }
    }

    private void findColumnIndexes(JSONArray mapDetailData) throws JSONException {
        JSONArray indexesArray = mapDetailData.getJSONArray(0);
        for (int i = 0; i < indexesArray.length(); i++) {
            String columnName = indexesArray.getString(i);
            columnIndexes.put(columnName, i);
        }

        for (String columnName : COLUMN_NAMES) {
            if (!columnIndexes.containsKey(columnName))
                throw new JSONException("provided json data is missing at least one column name: " + columnName);
        }
    }

    private int getColumnIndex(String columnName) {
        Integer index = columnIndexes.get(columnName);
        if (index == null)
            throw new IllegalArgumentException("there is no known index of " + columnName);

        return index;
    }

    private boolean updateData(JSONArray mapDetailData, String newHash) throws JSONException {
        if (dataHash.equals(newHash))
            return false;

        mapDetailDao.deleteAllMapDetails();

        for (int i = 0; i < mapDetailData.length(); i++) {
            JSONArray mapDetailJSON = mapDetailData.getJSONArray(i);

            String id = mapDetailJSON.getString(getColumnIndex(COLUMN_NAME_ID));
            String title = mapDetailJSON.getString(getColumnIndex(COLUMN_NAME_TITLE));
            String message = mapDetailJSON.getString(getColumnIndex(COLUMN_NAME_MESSAGE));

            MapDetail mapDetail = mapDetailDao.getMapDetail(id);
            if (mapDetail != null) {
                if (!mapDetail.getTitle().equals(title) ||
                        !mapDetail.getMessage().equals(message)) {
                    mapDetail.setTitle(title);
                    mapDetail.setMessage(message);
                    mapDetailDao.update(mapDetail);
                }
            } else {
                mapDetail = new MapDetail(id, title, message);
                mapDetailDao.insert(mapDetail);
            }
        }

        sharedPreferences.edit().putString(KEY_DATA_HASH, newHash).apply();
        return true;
    }
}
